#include <Wire.h>
#include "PS2X_lib.h"  //for v1.6
#include <Servo.h>   //include servo lib
#include <AFMotor.h>

/******************************************************************
   set pins connected to PS2 controller:
     - 1e column: original`
     - 2e colmun: Stef?
   replace pin numbers by the ones you use
 ******************************************************************/
/******************************************************************
   select modes of PS2 controller:
     - pressures = analog reading of push-butttons
     - rumble    = motor rumbling
   uncomment 1 of the lines for each mode selection
 ******************************************************************/
#define pressures   true
#define rumble      true

#define PS2_DAT        24
#define PS2_CMD        26
#define PS2_SEL        28
#define PS2_CLK        22

#define SVR_FB         4
#define SVR_GRAB       2
#define SVR_ROT        7
#define SVR_ALT        11

#define AIN1         5
#define AIN2         6
#define BIN1         10
#define BIN2         9

Servo servo_FB;
Servo servo_Grab;
Servo servo_rot;
Servo servo_alt;

int angle_alt = 90;
int angle_rot = 90;
int angle_Grab = 90;
int angle_FB = 90;

PS2X ps2x; // create PS2 Controller Class

//right now, the library does NOT support hot pluggable controllers, meaning
//you must always either restart your Arduino after you connect the controller,
//or call config_gamepad(pins) again after connecting the controller.

int error = 0;
byte type = 0;
byte vibrate = 0;

void setMotor(int MOTORA, int MOTORB) {
  if (MOTORA >= 0)
  {
    digitalWrite(AIN2, HIGH);
    analogWrite(AIN1, 255 - MOTORA);
  }
  else
  {
    digitalWrite(AIN1, HIGH);
    analogWrite(AIN2, MOTORA + 255);
  }
  if (MOTORB >= 0)
  {
    digitalWrite(BIN2, HIGH);
    analogWrite(BIN1, 255 - MOTORB);
  }
  else
  {
    digitalWrite(BIN1, HIGH);
    analogWrite(BIN2, 255 + MOTORB);
  }
}

void setup() {
  Serial.begin(57600);

  pinMode(AIN1, OUTPUT);
  pinMode(AIN2, OUTPUT);
  pinMode(BIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);

  servo_FB.attach(SVR_FB);
  servo_Grab.attach(SVR_GRAB);
  servo_rot.attach(SVR_ROT);
  servo_alt.attach(SVR_ALT);
  Serial.println("All four servo motors have been configured.");

  delay(300);  //added delay to give wireless ps2 module some time to startup, before configuring it

  //  //CHANGES for v1.6 HERE!!! **************PAY ATTENTION*************
  //
  //  //setup pins and settings: GamePad(clock, command, attention, data, Pressures?, Rumble?) check for error
  //  error = ps2x.config_gamepad(PS2_CLK, PS2_CMD, PS2_SEL, PS2_DAT, pressures, rumble);
  //
  do {
    //setup pins and settings: GamePad(clock, command, attention, data, Pressures?, Rumble?) check for error
    error = ps2x.config_gamepad(PS2_CLK, PS2_CMD, PS2_SEL, PS2_DAT, pressures, rumble);
    if (error == 0) {
      Serial.println("Configured successful");
      break;
    } else {
      Serial.print(".");
      delay(100);
    }
  } while (1);

  type = ps2x.readType();
  switch (type) {
    case 0:
      Serial.println("Unknown Controller type found ");
      break;
    case 1:
      Serial.println("DualShock Controller found ");
      break;
    case 2:
      Serial.println("GuitarHero Controller found ");
      break;
    case 3:
      Serial.println("Wireless Sony DualShock Controller found ");
      break;
  }
}

void loop() {
  /* You must Read Gamepad to get new values and set vibration values. ps2x.read_gamepad(small motor on/off, larger motor strenght from 0-255).
     If you don't enable the rumble, use ps2x.read_gamepad(); with no values.
     You should call this at least once a second
  */

  servo_alt.write(angle_alt);
  servo_rot.write(angle_rot);
  servo_Grab.write(angle_Grab);
  servo_FB.write(angle_FB);

  int ly = (((ps2x.Analog(PSS_LY)) - 127) * 100) / 128 ;
  int ry = (((ps2x.Analog(PSS_RY)) - 127) * 100) / 128 ;

  setMotor(ry, ly);

  if (error == 1) //skip loop if no controller found
    return;

  //DualShock Controller
  ps2x.read_gamepad(false, vibrate); //read controller and set large motor to spin at 'vibrate' speed

  if (ps2x.Button(PSB_START))       //will be TRUE as long as button is pressed
    Serial.println("Start is being held");

  if (ps2x.ButtonPressed(PSB_SELECT)) {
    Serial.println("Stick Values:");
    Serial.print("LeftY");
    Serial.print(":");
    Serial.print(ps2x.Analog(PSS_LY), DEC);
    Serial.println(",");
    Serial.print("LeftX");
    Serial.print(":");
    Serial.print(ps2x.Analog(PSS_LX), DEC);
    Serial.println(",");
    Serial.print("RightY");
    Serial.print(":");
    Serial.print(ps2x.Analog(PSS_RY), DEC);
    Serial.println(",");
    Serial.print("RightX");
    Serial.print(":");
    Serial.println(ps2x.Analog(PSS_RX), DEC);
  }

  if (ps2x.Button(PSB_PAD_UP)) {              //will be TRUE as long as button is pressed
    if (angle_FB < 170) {
      angle_FB = angle_FB + 1;
      Serial.println("Down");
    }
  }
  if (ps2x.Button(PSB_PAD_RIGHT)) {
    if (angle_rot > 0) {
      angle_rot = angle_rot - 1;
      Serial.println("Right");
    }
  }
  if (ps2x.Button(PSB_PAD_LEFT)) {
    if (angle_rot < 170) {
      angle_rot = angle_rot + 1;
      Serial.println("Left");
    }
  }
  if (ps2x.Button(PSB_PAD_DOWN)) {
    if (angle_FB > 0) {
      angle_FB = angle_FB - 1;
      Serial.println("Up");
    }
  }

  if (ps2x.Button(PSAB_CROSS));
  vibrate = 0;

  if (ps2x.Button(PSB_L3))
    Serial.println("L3 pressed");

  if (ps2x.Button(PSB_R3))
    Serial.println("R3 pressed");

  if (ps2x.Button(PSB_L2)) {
    Serial.println("Grab angle: ");
    Serial.println(angle_Grab);
    if (angle_Grab > 0) {
      angle_Grab = angle_Grab - 1;
    }
  }
  if (ps2x.Button(PSB_R2)) {
    Serial.println("Grab angle: ");
    Serial.println(angle_Grab);
    if (angle_Grab < 101) {
      angle_Grab = angle_Grab + 1;
    }
  }
  if (ps2x.Button(PSB_TRIANGLE))
    Serial.println("Triangle pressed");

  if (ps2x.ButtonPressed(PSB_CIRCLE))              //will be TRUE if button was JUST pressed
    Serial.println("Circle just pressed");
  if (ps2x.NewButtonState(PSB_CROSS))              //will be TRUE if button was JUST pressed OR released
    Serial.println("State of X just changed");
  if (ps2x.ButtonReleased(PSB_SQUARE))             //will be TRUE if button was JUST released
    Serial.println("Square just released");

  if (ps2x.Button(PSB_L1)) {
    Serial.print("Angle: ");
    Serial.println(angle_alt);
    if (angle_alt < 145) {
      angle_alt = angle_alt + 1;
    }
  }
  if (ps2x.Button(PSB_R1)) {
    Serial.print("Angle: ");
    Serial.println(angle_alt);
    if (angle_alt > 0) {
      angle_alt = angle_alt - 1;
    }
  }
  delay(50);
}
